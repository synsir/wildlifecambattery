#include <synos.h>

//#include "../stmbase/drivers/tm1638.h"
//#include "../stmbase/drivers/nrf24/rf24node.h"
//#include "../stmbase/drivers/ds18b20/ds18b20.h"

//#include <stdio.h>
//#define printf(x, y) (x, y)
//#define printf(x) (x)

syn::Led alive_led;

//DS18B20Manager temp_manager;


void check_voltage(uint16_t arg)
{
  // uses port b5 (I2C)
  alive_led.init();
  alive_led.clear();

  // voltage divider
  // bat+ ---- 69k
  //            |
  //          sense
  //            |
  // bat- ---- 50k8

  syn::Adc::turnOn();
  while(1)
  {
    // pin D 3
    uint16_t volt = syn::Adc::readSingle(4);
    // total divider is 119k8
    // battery 50% voltage is about 6.2 Volt
    // 6.2 / 119.8 * 50.8 = 2.63 V on sense
    // ADC is 10 bit resolution with 3.3V range
    // LSB / Volt = 1024 / 3.3
    // compare value = 2.63 * (1024 / 3.3) = 816
    if (volt < 820)
    {
      alive_led.set(true); // turn on warning LED
    }
    else
    {
      alive_led.clear();
    }
    
    syn::Autowakeup::sleep(30000);
    //alive_led.toggle();
    //syn::Routine::sleep(10);
  }
}

void thermometer_routine(uint16_t arg)
{
  //uint8_t count = temp_manager.init('D', 4);

  while (1)
  {
    syn::Routine::sleep(1000);
  }
}

int main()
{
  syn::Kernel::init();

  // IMPORTANT !!!
  // the count of routines has to exactly match the amount of routines used
  // the routine index given during initialization has to match

  // Routines should never return, but there is a safeguard if that happens.
  // However that safeguard costs 2 bytes of stack and 6 bytes of rom.
  // furthermore, if roundrobin is disabled, the processor will be stuck on the
  // returned rotuine forever. you have been warned.

  // routine index 0, round_robin_time 10 millis
  // stack allocated automatically
  syn::Routine::init(&check_voltage, 0, 128);

  // routine index 1, managing the nrf24 module
  // RF24Node::Config nrfcfg;
  // nrfcfg.this_node_address = RF24_NODE_FOLD_ADDR('D', 'G', 1);
  // nrfcfg.indata_handler = 0;
  // syn::Routine::init(&RF24Node::message_handler_routine, (uint16_t)&nrfcfg, 128);

  //syn::Routine::init(&thermometer_routine, 0, 128);

  //syn::SysTickHook::init<0, 10>(tim_x);
  //syn::SysTickHook::init<1, 10>(tim_y);

  // never returns
  syn::Kernel::spin();
  return 0;
}
